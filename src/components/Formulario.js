import React,{useContext,useState} from 'react';
import {CategoriasContext} from '../context/CatogoriaContext';
import { RecetasContext} from '../context/RecetasContext';

const Formulario = () => {

    //state para lo que digita el usuario
    const [busqueda,guardarBusqueda]=useState({
        nombre:'',
        categoria:''
    });
    const {guardarBusquedaReceta,guardarConsultar}= useContext(RecetasContext);
    const {categorias} = useContext(CategoriasContext);
    
    //funcion oara leer los contenidos

    const obtenerDatosRecetas=e=>{
        guardarBusqueda({
            ...busqueda,
            [e.target.name] : e.target.value
        })
    }
    

    return ( 
        <form
            className="col-12"
            onSubmit={e=>{
                e.preventDefault();
                guardarBusquedaReceta(busqueda);
                guardarConsultar(true);
            }}
        >
            <fieldset className="text-center">
                <legend>Busca Bebidas por Categoría o Ingrediente</legend>
            </fieldset>
            <div className="row">
                <div className="col-md-4">
                    <input
                        name="nombre"
                        className="form-control"
                        type="text"
                        placeholder="buscar por ingrediente"
                        onChange={obtenerDatosRecetas}
                    />
                </div>
                <div
                className="col-md-4">
                    <select
                        className="form-control"
                        name="categoria"
                        onChange={obtenerDatosRecetas}
                    >
                        <option value="">--Selecciona Categoría--</option>
                        {categorias.map(categoria=>(
                            <option
                                value={categoria.strCategory}
                                key={categoria.strCategory}
                            >{categoria.strCategory}</option>
                        ))}
                    </select>
                </div>
                <div className="col-md-4">
                    <input
                        type="submit"
                        className="btn btn-block btn-primary"
                        value="Buscar Bebidas"
                    />
                </div>
            </div>
        </form>

     );
}
 
export default Formulario;