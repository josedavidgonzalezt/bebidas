import React, {createContext, useState, useEffect} from 'react';
import axios from 'axios';
//crea el context

export const CategoriasContext = createContext();

//Provider es donde se encuentran las funciones y states
const CategoriasProvider = (props)=>{
     
    //crear el state del context
    const [categorias, guardarCategorias] = useState([]);

    //ejecutar el llamado a la api
    useEffect(()=>{
        const obtnerCategorias= async()=>{
            const url=`https://www.thecocktaildb.com/api/json/v1/1/list.php?c=list`;
            const categorias= await axios(url);
            guardarCategorias(categorias.data.drinks);
        }
        obtnerCategorias()

    },[]);

    return(
        <CategoriasContext.Provider
            value={{
                categorias,
            }}
        >
            {props.children}
        </CategoriasContext.Provider>
    )

}
export default CategoriasProvider;