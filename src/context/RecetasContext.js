import React, { createContext, useState, useEffect } from 'react';
import axios from 'axios';

export const RecetasContext= createContext();

const RecetasProvider = (props) => {

    const [recetas, guardarRecetas] = useState([])
    const [busquedareceta,guardarBusquedaReceta]= useState({
        nombre:'',
        categoria:''
    });

    const [consultar, guardarConsultar] = useState(false);

    const {nombre,categoria}= busquedareceta;

    useEffect(()=>{

        if (consultar){
            const ObtenerRecetas = async ()=>{

                const url =`https://www.thecocktaildb.com/api/json/v1/1/filter.php?i=${nombre}&c=${categoria}`;

                const receta = await axios(url);
                /* console.log(receta.data.drinks); */

                guardarRecetas(receta.data.drinks);
            }
            ObtenerRecetas()
        }
        
        
    },[busquedareceta,nombre,categoria,consultar])

    return ( 
        <RecetasContext.Provider
            value={{
                recetas,
                guardarBusquedaReceta,
                guardarConsultar
            }}
        >
            {props.children}
        </RecetasContext.Provider>
     );
}
 
export default RecetasProvider;